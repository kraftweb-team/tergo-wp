<article @php post_class() @endphp>
  @php
    $post_title = get_the_title();
    $post_url = get_permalink();
  @endphp

  <div class="teaser">
    <div class="inner">
      <a href="{{ $post_url }}" title="{{ $post_title }}">
        <div class="image-wrapper">
          <img src="{{ the_field('hero_image') }}" alt="{{ $post_title }} - {{ get_bloginfo('description') }}"/>
        </div>
      </a>
      <div class="uk-flex uk-flex-column uk-padding uk-text-center uk-text-left@m details">
        <a href="{{ $post_url }}" title="{{ $post_title }}">
          <h2 class="title">{{ $post_title }}</h2>
        </a>
        @if (get_field('teaser_title'))
          <div class="desc">{{ the_field('teaser_title') }}</div>
        @endif
        @if (get_field('read_time'))
          <div class="time">{{ the_field('read_time') }} {{ get_locale() === 'en_US' ? 'min read' : 'min czytania' }}</div>
        @endif
        <div>
          <a class="uk-button" href="{{ $post_url }}" title="{{ $post_title }}">{{ get_locale() === 'en_US' ? 'Read more' : 'Czytaj więcej' }}<img src="@asset('images/arrow_white.svg')" /></a>
        </div>
      </div>
    </div>
  </div>
</article>
