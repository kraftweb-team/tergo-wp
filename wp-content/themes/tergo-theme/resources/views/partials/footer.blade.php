<footer class="footer">
  <div class="uk-container uk-flex uk-flex-between uk-flex-wrap">
    <div class="uk-width-1-1 uk-width-1-4@m uk-flex-column">
      {{ the_field('footer_text', (get_locale() === 'en_US') ? 24 : 74 ) }}
    </div>
    <div class="uk-width-1-1 uk-width-1-4@m socials uk-text-center uk-text-left@m">
      <h5>Visit us:</h5>

      <div class="uk-flex uk-flex-center uk-flex-left@m">
        <a href="https://www.facebook.com/TerGo-105025195065313" target="_black"
          class="uk-icon-button uk-margin-right" uk-icon="facebook"></a>
        <!-- <a href="#" target="_black" class="uk-icon-button uk-margin-right" uk-icon="twitter"></a> -->
        <a href="https://www.instagram.com/tergo_io/ " target="_black" class="uk-icon-button uk-margin-right"
          uk-icon="instagram"></a>
        <a href="https://www.linkedin.com/company/tergo/" target="_black" class="uk-icon-button"
          uk-icon="linkedin"></a>
      </div>
    </div>
  </div>
  <div class="uk-container uk-flex uk-flex-between uk-flex-wrap">
    <div class="uk-width-1-1 uk-width-1-3@m">
      <div class="uk-flex-inline uk-flex-around uk-flex-left@m uk-width-1-1 uk-width-auto@m">
        @php apply_filters( 'wpml_element_link', 200 ) @endphp
        <div class="divider">|</div>
        @php apply_filters( 'wpml_element_link', 209 ) @endphp
      </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-3@m uk-flex-first@m uk-text-center uk-text-left@m">
      <span>TerGo 2021</span>
    </div>
  </div>
</footer>
