<article @php post_class() @endphp>
  <section class="uk-section top">
    <div class="uk-container uk-padding">
      <h1>{{ get_the_title() }}</h1>
      <p class="uk-margin-top">{{ the_field('teaser_title') }}</p>
      <p class="read-time">{{ the_field('read_time') }} {{ get_locale() === 'en_US' ? 'min read' : 'min czytania' }}</p>

      <a href="#my-id" uk-scroll>
        <img class="scroll-down" src="@asset('images/scroll_down_button.svg')" />
      </a>
    </div>

    <div class="bg" style="background-image: url('{{ the_field('hero_image') }}')"></div>
  </section>

  <section class="uk-section uk-padding uk-padding-remove-horizontal" id="my-id">
    <div class="uk-container uk-flex uk-flex-left uk-flex-wrap">
      <div class="uk-width-1-1 uk-width-1-3@m details">
        <div class="categories">
          <div class="uk-flex uk-flex-left uk-flex-right@m uk-flex-middle uk-flex-middle uk-flex-wrap">
            <h4 class="uk-width-auto uk-width-1-1@m">{{ get_locale() === 'en_US' ? 'Categories' : 'Kategorie' }}:</h4>

            @php
              $categories = get_the_category();
            @endphp

            @foreach ($categories as $category)
              <a href="{{ esc_url(get_category_link($category->term_id)) }}" class="uk-button uk-width-auto uk-margin-small-bottom uk-margin-small-left">{{ htmlspecialchars_decode($category->name) }}</a>
            @endforeach

          </div>
        </div>

        <div class="author uk-flex uk-flex-left uk-flex-right@m uk-flex-wrap">
          <h4 class="uk-width-auto uk-width-1-1@m">{{ get_locale() === 'en_US' ? 'Author' : 'Autor' }}:</h4>
          <h4>{{ the_field('author') }}</h4>
          <p class="uk-width-1-1">{{ the_field('author_description') }}</p>
        </div>

        <div class="quote">
          <img src="@asset('images/quote-top.png')" />
          <h4>{{ the_field('quote') }}</h4>
          <img src="@asset('images/quote-bottom.png')" />
        </div>
      </div>
      <div class="uk-width-1-1 uk-width-2-3@m">
        <div class="breadcrumbs uk-margin-medium-bottom">
          @php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); @endphp
        </div>
        @php the_content(); @endphp
      </div>
    </div>
  </section>

  <section class="uk-section tags">
    <div class="uk-container uk-flex uk-flex-left uk-flex-wrap">
      <div class="uk-width-1-1 uk-width-1-3@m">
        <div class="tags">
          <h4>{{ get_locale() === 'en_US' ? 'Tags' : 'Tagi' }}:</h4>
          <div class="uk-flex uk-flex-left uk-flex-right@m uk-flex-wrap">
            @php
              $tags = get_the_tags();
            @endphp

            @foreach ($tags as $tag)
              <a href="{{ esc_url(get_tag_link( $tag->term_id)) }}" class="uk-button uk-width-auto uk-margin-small-bottom">{{ htmlspecialchars_decode( $tag->name ) }}</a>
            @endforeach
          </div>
        </div>
      </div>
      <div class="uk-width-1-1 uk-width-2-3@m"></div>
    </div>
  </section>
</article>
