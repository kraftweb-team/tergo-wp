@php
  $url = get_the_permalink();
  $currentLang = get_locale();
@endphp

<header class="headroom headroom--top uk-flex uk-flex-between uk-flex-left@m uk-flex-middle">
  <a href="{{ home_url('/') }}"><img class="logo" src="@asset('images/tergo_logo_big.png')" /></a>

  <div id="offcanvas-nav">
    <div>
      <a href="{{ home_url('/') }}" class="uk-hidden@m"><img class="logo" src="@asset('images/tergo_logo_big.png')" /></a>
      <span class="uk-offcanvas-close uk-hidden@m" uk-icon="icon: close; ratio: 2"></span>

      {{ clean_custom_menu("primary_navigation") }}
    </div>
  </div>

  <span uk-toggle="target: #offcanvas-nav" uk-icon="icon: menu; ratio: 2" class="uk-hidden@m"></span>

  <div class="lang">
    @if ($currentLang === 'en_US')
      <img src="@asset('images/lang_icon.svg')" class="uk-margin-right" /><strong>ENG</strong> | <a href="{{ get_lang_post('pl') }}">PL</a>
    @else
      <img src="@asset('images/lang_icon.svg')" class="uk-margin-right" /><a href="{{ get_lang_post('en') }}">ENG</a> | <strong>PL</strong>
    @endif
  </div>
</header>
