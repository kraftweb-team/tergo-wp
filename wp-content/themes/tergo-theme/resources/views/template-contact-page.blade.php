{{--
  Template Name: Contact Page Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile

  <div class="uk-container uk-text-center done uk-hidden">
    <h2 class="uk-margin-medium-top">Message sent successfully. Thank you!</h2>

    <img src="@asset('images/form_send_done.png')" class="uk-display-block uk-align-center uk-margin-medium-top" />

    <a href="{{ get_home_url() }}" class="uk-button uk-margin-top">
      Return to homepage<img src="@asset('images/arrow_white.svg')" />
    </a>
  </div>
  <div class="uk-container uk-text-center fail uk-hidden">
    <h2 class="uk-margin-medium-top">Oops. Something went wrong!</h2>

    <img src="@asset('images/form_send_fail.png')" class="uk-display-block uk-align-center uk-margin-medium-top" />

    <a href="{{ get_permalink() }}" class="uk-button centered uk-margin-top">
      Try again<img src="@asset('images/arrow_white.svg')" />
    </a>
  </div>

  <script>
    var wpcf7Elm = document.querySelector('.wpcf7');

    wpcf7Elm.addEventListener('wpcf7mailsent', function (event) {
      document.querySelector('.uk-section.top').classList.add('uk-hidden');
      document.querySelector('.uk-container.done').classList.remove('uk-hidden');
    }, false );

    wpcf7Elm.addEventListener('wpcf7mailfailed', function (event) {
      document.querySelector('.uk-section.top').classList.add('uk-hidden');
      document.querySelector('.uk-container.done').classList.remove('uk-hidden');
    }, false );
  </script>
@endsection
