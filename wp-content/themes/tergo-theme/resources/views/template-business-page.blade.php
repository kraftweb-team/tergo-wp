{{--
  Template Name: Business Template
--}}

@extends('layouts.app')

@section('content')
<section class="uk-section uk-text-center uk-padding top">
  @if (function_exists('rank_math_the_breadcrumbs'))
  <div class="uk-container breadcrumbs uk-margin-medium-bottom">
    {{ rank_math_the_breadcrumbs() }}
  </div>
  @endif

  {{ the_field('hero_text') }}

  <a href="https://betawww.tergo.io/business/business-form" class="uk-button centered uk-margin-medium-top">
    {{ the_field('hero_button') }}<img src="@asset('images/arrow_white.svg')" />
  </a>
</section>

<section class="uk-section yellow">
  <div class="uk-container uk-padding uk-text-center uk-text-left@m">
    {{ the_field('section_1') }}
  </div>
</section>

<section class="uk-section">
  <a id="1"></a>
  <div class="uk-container uk-padding uk-flex uk-flex-between uk-text-center uk-text-left@m">
    <div class="uk-flex uk-flex-column">
      {{ the_field('section_2') }}
    </div>
  </div>
</section>

<section class="uk-section">
  <a id="2"></a>
  <div class="uk-container uk-padding uk-text-center uk-text-left@m">
    {{ the_field('section_3') }}
  </div>
</section>

<section class="uk-section gray">
  <a id="3"></a>
  <div class="uk-container uk-padding uk-text-center uk-text-left@m">
    {{ the_field('section_4') }}
  </div>
</section>

<section class="uk-section">
  <a id="4"></a>
  <div class="uk-container uk-padding uk-text-center uk-text-left@m uk-flex uk-flex-between uk-flex-wrap">
    {{ the_field('section_5') }}
  </div>
</section>

<section class="uk-section yellow">
  <a id="5"></a>
  <div class="uk-container uk-padding uk-text-center uk-text-left@m uk-flex uk-flex-between uk-flex-wrap">
    {{ the_field('section_6') }}
  </div>
</section>

<section class="uk-section gray">
  <a id="6"></a>
  <div class="uk-container uk-padding uk-text-center uk-text-left@m uk-flex uk-flex-between uk-flex-wrap">
    <div class="uk-width-1-1">
      {{ the_field('section_7') }}
    </div>
  </div>
</section>
@endsection

