{{--
  Template Name: Ters Page Template
--}}

@extends('layouts.app')

@section('content')


<section class="uk-section uk-padding uk-text-center light-blue top" >
  @if (function_exists('rank_math_the_breadcrumbs'))
    <div class="uk-container breadcrumbs uk-margin-medium-bottom">
      {{ rank_math_the_breadcrumbs() }}
    </div>
  @endif
  <div class="uk-width-1-1 uk-padding">
    {{ the_field('hero_text') }}

    <div class="uk-margin-medium-top uk-margin-auto" style="position:relative; width: 100%; max-width: 480px; height: 270px; border: 2px solid #000;"><iframe width="100%" height="100%" src="{{ the_field('hero_embed_video') }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
</section>

<section class="uk-section uk-padding-remove">
  <div class="uk-container uk-padding">
    {{ the_field('accordion') }}
  </div>
</section>

<section class="uk-section"><a id="terbit"></a>
  <div
    class="uk-container uk-padding uk-flex uk-flex-between uk-flex-middle uk-flex-wrap uk-text-center uk-text-left@m">
    @if (get_field('section_1_image'))
      <img src="{{ the_field('section_1_image') }}" class="uk-width-1-1 uk-width-1-2@m" />
    @endif

    <div class="uk-flex uk-flex-column uk-width-1-1 uk-width-1-2@m">
      {{ the_field('section_1_text') }}
    </div>
  </div>
</section>

<section class="uk-section uk-padding-remove">
  <div
    class="uk-container uk-padding uk-flex uk-flex-between uk-flex-middle uk-flex-wrap uk-text-center uk-text-left@m">

    @if (get_field('section_2_image'))
      <img src="{{ the_field('section_2_image') }}" class="uk-width-1-1 uk-width-1-2@m" />
    @endif

    <div class="uk-flex uk-flex-column uk-width-1-1 uk-width-1-2@m">
      {{ the_field('section_2_text') }}
    </div>
  </div>
</section>

<section class="uk-section uk-padding-remove">
  <div
    class="uk-container uk-padding uk-flex uk-flex-between uk-flex-middle uk-flex-wrap uk-text-center uk-text-left@m">

    @if (get_field('section_3_image'))
      <img src="{{ the_field('section_3_image') }}" class="uk-width-1-1 uk-width-1-2@m" />
    @endif

    <div class="uk-flex uk-flex-column uk-width-1-1 uk-width-1-2@m">
      {{ the_field('section_3_text') }}
    </div>
  </div>
</section>
@endsection
