@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @if (function_exists('rank_math_the_breadcrumbs'))
    <div class="uk-container breadcrumbs uk-margin-medium-bottom uk-margin-medium-top">
      {{ rank_math_the_breadcrumbs() }}
    </div>
    @endif

    @include('partials.page-header')
    @include('partials.content-page')
  @endwhile
@endsection
