{{--
  Template Name: Home Page Template
--}}

@extends('layouts.app')

@section('content')
<section class="uk-section uk-text-center light-blue top">
  <div class="uk-width-1-1 uk-padding">

    {{ the_field('hero_text') }}

    <div class="form-wrapper">
      <form id="newsletter">
        <div class="uk-flex uk-flex-left uk-flex-middle uk-flex-wrap uk-form-controls">
          <input type="email" name="user_email" required class="uk-input uk-width-1-1" id="user_email" placeholder="E-mail">
          <button type="submit" class="uk-button">Sign up</button>
        </div>
      </form>
    </div>
  </div>

  @if (get_field('hero_video'))
    <video autoplay loop muted>
      <source src="{{ the_field('hero_video') }}" type="video/mp4">
    </video>
  @endif

  <div class="wall-wrapper">
    <div class="wall uk-flex uk-flex-column">
      @if (get_field('hero_image'))
        <img src="{{ the_field('hero_image') }}" />
      @endif
    </div>
    <div class="drag-button"></div>
  </div>
</section>

<a id="about-us"></a>
<section class="uk-section uk-flex uk-flex-between uk-flex-stretch uk-flex-wrap pink uk-text-center uk-text-left@m">
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    <div class="inner uk-padding">
      {{ the_field('section_1_text') }}
    </div>
  </div>
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-center uk-flex-middle">
    @if (get_field('section_1_image'))
      <img src="{{ the_field('section_1_image') }}" />
    @endif
  </div>
</section>

<section
  class="uk-section uk-flex uk-flex-between uk-flex-stretch uk-flex-wrap light-blue uk-text-center uk-text-left@m">
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle uk-flex-last@m">
    <div class="inner uk-padding">
      {{ the_field('section_2_text') }}
    </div>
  </div>

  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    @if (get_field('section_2_image'))
      <img src="{{ the_field('section_2_image') }}" />
    @endif
  </div>
</section>

<section class="uk-section yellow uk-text-center uk-text-left@m">
  <div class="uk-container uk-padding">
    {{ the_field('section_3_text') }}

    @php
      $url = get_the_permalink(100);

      $langCode = (get_locale() === 'en_US') ? 'en' : 'pl';

      $wpml_permalink = apply_filters( 'wpml_permalink', $url, $langCode );
    @endphp

    <a href="{{ $wpml_permalink }}" class="uk-button transparent uk-margin-medium-top">
      Learn more<img src="@asset('images/arrow_black.svg')" />
    </a>
  </div>
</section>

<a id="app"></a>
<section class="uk-section gray uk-flex uk-flex-between uk-flex-stretch uk-flex-wrap uk-text-center uk-text-left@m">
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    <div class="inner uk-padding">
      {{ the_field('section_4_text') }}
    </div>
  </div>
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    @if (get_field('section_4_image'))
      <img src="{{ the_field('section_4_image') }}" />
    @endif
  </div>
</section>

<section class="uk-section uk-flex uk-flex-between uk-flex-stretch uk-flex-wrap yellow uk-text-center uk-text-left@m">
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle uk-flex-last@m">
    <div class="inner uk-padding">
      {{ the_field('section_5_text') }}
    </div>
  </div>
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    @if (get_field('section_5_image'))
      <img src="{{ the_field('section_5_image') }}" />
    @endif
  </div>
</section>

<a id="vers"></a>
<section
  class="uk-section uk-flex uk-flex-between uk-flex-stretch uk-flex-wrap light-blue uk-text-center uk-text-left@m" style="background-color: #429ddc">
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    <div class="inner uk-padding">
      {{ the_field('section_6_text') }}
    </div>
  </div>
  <div class="uk-width-1-1 uk-width-1-2@m uk-flex uk-flex-column uk-flex-center uk-flex-middle">
    @if (get_field('section_6_image'))
      <img src="{{ the_field('section_6_image') }}" />
    @endif
  </div>
</section>
@endsection
