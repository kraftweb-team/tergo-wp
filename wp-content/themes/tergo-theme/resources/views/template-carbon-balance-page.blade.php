{{--
  Template Name: Carbon Balance Template
--}}

@extends('layouts.app')

@section('content')
<section class="uk-section uk-text-center uk-padding top">
  @if (function_exists('rank_math_the_breadcrumbs'))
    <div class="uk-container breadcrumbs uk-margin-medium-bottom">
      {{ rank_math_the_breadcrumbs() }}
    </div>
  @endif
  {{ the_field('hero_text') }}
</section>

<section class="uk-section light-blue">
  <div class="uk-container uk-padding uk-flex uk-flex-between uk-flex-middle">
    <div class="uk-flex uk-flex-column uk-text-center uk-text-left@m">
      {{ the_field('step_1_text') }}

      <div class="uk-flex uk-flex-between uk-flex-bottom uk-flex-wrap">
        <div class="uk-width-1-1 uk-width-1-2@m">
          @if (get_field('calculator_image'))
            <img src="{{ the_field('calculator_image') }}" />
          @endif

          {{ the_field('calculator_text') }}

          <a class="uk-button blocked uk-margin-top">
            Option coming soon<img src="@asset('images/arrow_white.svg')" />
          </a>
        </div>
        <div class="uk-width-1-1 uk-width-1-2@m">
          @if (get_field('offset_image'))
            <img src="{{ the_field('offset_image') }}" />
          @endif

          {{ the_field('offset_text') }}

          <a class="uk-button blocked uk-margin-top">
            Option coming soon<img src="@asset('images/arrow_white.svg')" />
          </a>
        </div>
      </div>

      <div class="uk-margin-large-top uk-text-center">
        <p>Join the waiting list – be the first to know about our calculator launch!</p>

        <div class="form-wrapper">
          <form action="." method="post" id="newsletter">
            <div class="uk-flex uk-flex-left uk-flex-middle uk-flex-wrap uk-form-controls">

              <input type="email" name="user_email" required class="uk-input uk-width-1-1" id="user_email" placeholder="E-mail">
              <button type="submit" class="uk-button">Sign up</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="uk-section uk-padding-remove-bottom"><a id="krok-1"></a>
  <div class="uk-container uk-padding uk-flex uk-flex-between uk-flex-middle uk-flex-wrap">
    <div class="uk-width-1-1">{{ the_field('step_2_text') }}</div>

    @if (get_field('step_2_section_1_image'))
      <img src="{{ the_field('step_2_section_1_image') }}" class="uk-width-1-1 uk-width-1-2@m" />
    @endif

    <div class="uk-flex uk-flex-column uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
      {{ the_field('step_2_section_1_text') }}
    </div>
  </div>
</section>

<section class="uk-section uk-padding-remove">
  <div class="uk-container uk-padding uk-flex uk-flex-between uk-flex-middle uk-flex-wrap">
    @if (get_field('step_2_section_2_image'))
      <img src="{{ the_field('step_2_section_2_image') }}" class="uk-width-1-1 uk-width-1-2@m" />
    @endif

    <div class="uk-flex uk-flex-column uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
      {{ the_field('step_2_section_2_text') }}
    </div>
  </div>
</section>

<section class="uk-section uk-padding-remove">
  <div class="uk-container uk-padding uk-flex uk-flex-between uk-flex-middle uk-flex-wrap">
    @if (get_field('step_2_section_3_image'))
      <img src="{{ the_field('step_2_section_3_image') }}" class="uk-width-1-1 uk-width-1-2@m" />
    @endif

    <div class="uk-flex uk-flex-column uk-width-1-1 uk-width-1-2@m uk-text-center uk-text-left@m">
      {{ the_field('step_2_section_3_text') }}
    </div>
  </div>
</section>

<section class="uk-section gray">
  <div class="uk-container uk-padding">
    <div class="uk-flex uk-flex-column uk-text-center uk-text-left@m">
      {{ the_field('step_3_text') }}
    </div>
  </div>
</section>
@endsection
