@extends('layouts.app')

@php $term = get_queried_object(); $termID = $term->term_id; @endphp

@if (function_exists('rank_math_the_breadcrumbs'))
<div class="uk-container breadcrumbs uk-margin-top">
  {{ rank_math_the_breadcrumbs() }}
</div>
@endif

@include('partials.page-header')

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  <div class="uk-container uk-padding category-description">
    <p>@php echo category_description(); @endphp</p>

    @if (get_field('long_description', $term))
      <a href="#long-desc" class="uk-button centered uk-margin-large">
        Learn more<img src="@asset('images/arrow_white.svg')" />
      </a>
    @endif
  </div>

  <hr/>

  <div class="uk-container uk-margin-medium-top uk-flex uk-flex-wrap categories-list">
    @php
      $categories = get_categories();
    @endphp

    @foreach($categories as $category)
      <a href="{{ get_category_link($category->term_id) }}" class="uk-button {{ ($termID !== $category->term_id) ? 'white' : '' }} centered no-arrow uk-margin-right">{{ htmlspecialchars_decode($category->name) }}</a>
    @endforeach
  </div>

  <section class="uk-section uk-margin-large-top uk-margin-large-bottom">
    <div class="uk-container uk-padding-remove-horizontal">
      <div class="uk-grid">
          @while (have_posts()) @php the_post() @endphp
            <div class="uk-flex uk-flex-row uk-width-1-1 uk-width-1-2@m">
              @include('partials.content-'.get_post_type())
            </div>
          @endwhile

          @php wp_reset_postdata(); @endphp
      </div>
    </div>
  </section>

  @php
    $the_query = new WP_Query(array(
      'numberposts'	=> -3,
      'category' => $termID,
      'post_type'		=> 'post',
      'meta_query' => array(
        array(
          'key' => 'most_popular',
          'value' => '1',
        )
      )
    ));
  @endphp

  @if($the_query->have_posts())
    <hr/>

    <section class="uk-section uk-margin-large-top uk-margin-large-bottom">
      <div class="uk-container uk-padding-remove-horizontal">
        <h4 class="uk-margin-medium-top ">Most popular:</h4>

        <div class="uk-grid">
          @while( $the_query->have_posts() )
          @php $the_query->the_post(); @endphp
            <div class="uk-flex uk-flex-row uk-width-1-1 uk-width-1-3@m">
              @include('partials.content-'.get_post_type())
            </div>
          @endwhile

          @php wp_reset_query(); @endphp
        </div>
      </div>
    </section>
  @endif

  @if (get_field('long_description', $term))
    <div id="long-desc" class="uk-container uk-padding uk-padding-remove-horizontal uk-margin-medium-bottom category-long-description" style="border-top: 2px solid #000;">
      <h4>More information about this category:</h4>
      <p>{{ the_field('long_description', $term) }}</p>
    </div>
  @endif
@endsection


