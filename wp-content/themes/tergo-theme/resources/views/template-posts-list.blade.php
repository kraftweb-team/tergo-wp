{{--
  Template Name: Posts List Template
--}}

@extends('layouts.app')

@section('content')
  @php
    global $post;

    $posts = get_posts();

    $postsCount = count($posts);

    $firstPost = $posts[0];
    $firstPostID = $firstPost->ID;
  @endphp

  <section class="uk-section top" style="background-image: url('{{ the_field('hero_image', $firstPostID) }}')">
    @if (function_exists('rank_math_the_breadcrumbs'))
      <div class="uk-container breadcrumbs uk-margin-medium-bottom">
        {{ rank_math_the_breadcrumbs() }}
      </div>
    @endif

    <div class="uk-container uk-padding">
      <h1>{{ $firstPost->post_title }}</h1>
      <p>{{ the_field('teaser_title', $firstPostID) }}</p>
      <p class="read-time">{{ the_field('read_time', $firstPostID) }} {{ get_locale() === 'en_US' ? 'min read' : 'min czytania' }}</p>
      <a class="uk-button uk-width-auto" href="{{ get_permalink($firstPostID) }}">{{ get_locale() === 'en_US' ? 'Read more' : 'Czytaj więcej' }}<img
          src="@asset('images/arrow_white.svg')" /></a>
    </div>
  </section>

  <section class="uk-section uk-margin-large-top uk-margin-large-bottom">
    <div class="uk-container uk-padding-remove-horizontal">
      <div class="grid">
        <div class="uk-flex uk-flex-between">
          <div class="uk-flex uk-flex-column uk-width-1-1 uk-width-1-4@m">
            @for ($i = 2; $i < $postsCount; $i++)
              @php
                $post = $posts[$i];
                $postID = $post->ID;
              @endphp
              <div class="teaser small">
                <div class="inner">
                  <a href="{{ get_permalink($postID) }}">
                    <div class="image" style="background-image: url('{{ the_field('hero_image', $postID) }}')"></div>
                  </a>
                  <div class="uk-flex uk-flex-column uk-padding uk-text-center uk-text-left@m details">
                    <div class="title">{{ $post->post_title }}</div>
                    <div class="desc">{{ the_field('teaser_title', $postID) }}</div>
                    <div class="time">{{ the_field('read_time', $postID) }} {{ get_locale() === 'en_US' ? 'min read' : 'min czytania' }}</div>
                    <div>
                      <a class="uk-button" href="{{ get_permalink($firstPostID) }}">{{ get_locale() === 'en_US' ? 'Read more' : 'Czytaj więcej' }}<img src="@asset('images/arrow_white.svg')" /></a>
                    </div>
                  </div>
                </div>
              </div>
            @endfor
          </div>

          @php
            $secondPost = $posts[1];
            $secondPostID = $secondPost->ID;
          @endphp

          <div class="teaser big uk-width-1-1 uk-width-3-4@m">
            <div class="inner uk-flex uk-flex-column">
              <a href="{{ get_permalink($secondPostID) }}">
                <div class="image" style="background-image: url('{{ the_field('hero_image', $secondPost) }}')"></div>
              </a>
              <div class="uk-flex uk-flex-column uk-padding uk-text-left details">
                <div class="title">{{ $secondPost->post_title }}</div>
                <div class="desc">{{ the_field('teaser_title', $postID) }}</div>
                <div class="time">{{ the_field('read_time', $secondPostID) }} {{ get_locale() === 'en_US' ? 'min read' : 'min czytania' }}</div>
                <div>
                  <a class="uk-button" href="{{ get_permalink($secondPostID) }}">{{ get_locale() === 'en_US' ? 'Read more' : 'Czytaj więcej' }}<img
                      src="@asset('images/arrow_white.svg')" /></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @php wp_reset_postdata(); @endphp
@endsection
